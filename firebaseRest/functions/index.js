const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);


exports.requestNotification = functions.database
.ref("newOrders/{orderid}/receiver")
.onWrite(event => {
    var orderid=event.params.orderid;
  if (event.data.previous.exists()) {
    return;
  }
  if (!event.data.exists()) {
    return;
  }
  console.log("receiver " +event.data.val())
  var receiverid = event.data.val();
  console.log("Order ID: "+orderid)

return admin
     .database()
   .ref(`newOrders/`+orderid)
     .once("value")
     .then(odr=>{
         console.log("newOrders: "+odr.val())
        var item= odr.val().item;
        var table=odr.val().table;
        console.log("item: "+item + "table: " +table)
       
     })
  const getuser = admin
    .database()
    .ref(`tokens/`+receiverid)
    .once("value")
    .then(tokendata => {
        
      var token = tokendata.val();
        console.log("TOKEN: "+token);
      const payload = {
        notification: {
          title: `Table No. ${table}`,
          body: `item ordered  ${item}.`
        }
      };
      return admin
        .messaging()
        .sendToDevice(token, payload)
        .then(res => {
            console.log("Notifcation sent")
        });
    });
    return Promise.all([getuser]);
});